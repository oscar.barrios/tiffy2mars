Tiffi to Mars
=======================

### 1 - DEFINITION

We are sending Tiffi to Mars to give candies to Martians. For this purpose King has an agreement with
NASA. Tiffi and a robotic rover are going to land on a plateau on Mars. This plateau, which is curiously
rectangular, limited (9,9) and represented in a grid, must be navigated by the rover so Tiffi can give
candies to all Martians. The position and location of the rover is represented by a combination of x and y
co-‐ordinates and a letter representing one of the four cardinal compass points (N, S, E, W).
An example position might be 0, 0, N, which means the rover is in the bottom left corner and facing
North. In order to drive Tiffi around Mars, NASA sends a simple string of letters. The possible letters are
'L', 'R' and 'M'. 'L' and 'R' makes the rover spin 90 degrees left or right respectively, without moving the
rover from its current position. 'M' means move forward one grid point, and maintain the same heading.
Assume that the square directly North from (x, y) is (x, y+1).
Input: As input you get 2 strings, the first one is the position where the rover has landed; the second is the
sequence of movements the rover has to perform so that Tiffi can hand out the candies.
Output: As output you have to define Tiffi’s last position and orientation, so we can collect and bring her
back to earth to get more candies.

Example:
--------

Input:
- Landing position: 44N
- Movement planned: LMMMRRM

Expected Output:
- Last position: 24E

Please submit all of the following in one zip or tar.gz file:
- Source code
- Unit tests (the more the better, remember that you are applying for a Tester position)
- Validation against input data
- Demonstrated error handling
- Comments as deemed necessary
- Documentation (as necessary)
- Any assumptions that you have made

Optionally, you can also include a brief explanation of your solution (such as design
considerations).



### 2 - ARCHITECTURE

This application is implemented using an unique Class representing the "plateau", this class contains the representation of a grid  (X*Y, Orientation).
Supports getting methods to obtain the current position and orientation of tiffi, and a public method to perform the action established by a set of movements and an startup position.



### 3 - GETTING STARTED

Import Plateau Class to your code and call moveTiffi(String landingPosition, String movementPlaned) to obtain the last position of Tiffi.



### 4 - UNIT TESTS

You can run as an application org.king.tiffi2mars.backend.TestRunner or use Eclipse to run org.king.tiffi2mars.backend folder as a Junit4 Tests



### 5 - ACCEPTANCE TESTS using BDD (and JBehave framework)


LAUNCH TESTS:
In order to launch them, run src/test/acceptance/src/test/java/component/plateau/Movements/AcceptanceTests.java as a JunitTest

TEST REPORT:
Check the generated folder testreport, and open view/index.html in your web browser.
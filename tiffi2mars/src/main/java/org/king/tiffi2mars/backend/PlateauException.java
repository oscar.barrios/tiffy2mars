package org.king.tiffi2mars.backend;

/**
 * Main exception class. Represents any generic exception happened in runtime.
 * Any other exceptions are inherited from this one
 * 
 * @author Oscar Barrios (srbarrios@gmail.com)
 */
public class PlateauException extends Exception {

	private static final long serialVersionUID = 6248463987065087574L;
	private int error_code = 0;

	public int getError_code() {
		return error_code;
	}

	public PlateauException() {
	}

	public PlateauException(String message) {
		super(message);
	}

	public PlateauException(String message, int error_code) {
		super(message);
		this.error_code = error_code;
	}

	/*
	 * Examples:
	 * 
	 * - Wrong landing position - Unknown movement - Overboundary of the grid
	 * (X,Y) ranges
	 */
}

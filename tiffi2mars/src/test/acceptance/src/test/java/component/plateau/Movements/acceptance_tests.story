Movements of Tiffi in Mars

Narrative:
In order to implement correctly my application
As a checker of movements
I want to validate the different possibilities that could appears in the input actions

Scenario: Validate the happy path
Given a new Plateau
When I perform the [movements] starting on [landing_position]
Then I obtain the [last_position]

Examples:
        | index | landing_position | movements                  | last_position |
        | 0     | 44N              | LMMMRRM                    | 24E           |
        | 1     | 00N              | LLLLRRRRLLLLRRRR           | 00N           |
        | 2     | 99W              | MMMMMMMLRLRLRLRLLRLRLRRRRM | 39E           |
        | 3     | 35S              | MRMRMRMRMRMMMLMMMM         | 00S           |
        
    
Scenario: Validate overboundaries scenarios
Given a new Plateau
When I perform the [movements] starting on [landing_position]
Then I obtain an error_code [error_code]

Examples:
    | index | landing_position | movements | error_code |
    | 0     | 93N              | RMMMRRM   | 1          |
    | 1     | 99N              | MMMMRRM   | 1          |
    | 2     | 00W              | LMMMRRM   | 1          |
    | 3     | 00N              | LMMMRRM   | 1          |
    
    
Scenario: Validate wrong landing position
Given a new Plateau
When I perform the [movements] starting on [landing_position]
Then I obtain an error_code [error_code]

Examples:
    | index | landing_position | movements | error_code |
    | 0     | -93N             | RMMMRRM   | 5          |
    | 1     | 109N             | MMMMRRM   | 5          |
    | 2     | 00Z              | LMMMRRM   | 3          |

    
Scenario: Validate unknown movements
Given a new Plateau
When I perform the [movements] starting on [landing_position]
Then I obtain an error_code [error_code]

Examples:
    | index | landing_position | movements | last_position | error_code |
    | 0     | 44N              | LMXMRRM   | 24E           | 2          |
    | 1     | 44N              | LM?MRRM   | 24E           | 2          |
    | 2     | 44N              | LM MRRM   | 24E           | 2          |
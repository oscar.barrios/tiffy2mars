Folder for acceptance tests

### 1 - LAUNCH TESTS

In order to launch them, run src/test/acceptance/src/test/java/component/plateau/Movements/AcceptanceTests.java as a JunitTest
 
### 2 - TEST REPORT

Check the generated folder testreport, and open view/index.html in your web browser.
 
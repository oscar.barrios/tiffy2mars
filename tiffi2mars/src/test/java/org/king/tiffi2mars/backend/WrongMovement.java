/**
 * 
 */
package org.king.tiffi2mars.backend;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Oscar Barrios
 * 
 */
public class WrongMovement {

	private Plateau plateau;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.plateau = new Plateau();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.plateau = null;
	}

	/**
	 * Test method for
	 * {@link org.king.tiffi2mars.backend.Plateau#moveTiffi(String, String)}
	 * checking over boundary from the grid(plateau)
	 */
	@Test
	public final void testOverBoundaryGrid() {
		System.out.println("Over boundary from grid...");
		try {
			assertEquals("Overboundaries 1: ", "",
					this.plateau.moveTiffi("93N", "RMMMRRM"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
		try {
			assertEquals("Overboundaries 2: ", "",
					this.plateau.moveTiffi("99N", "MMMMRRM"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
		try {
			assertEquals("Overboundaries 3: ", "",
					this.plateau.moveTiffi("00W", "LMMMRRM"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
		try {
			assertEquals("Overboundaries 4: ", "",
					this.plateau.moveTiffi("00N", "LMMMRRM"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link org.king.tiffi2mars.backend.Plateau#moveTiffi(String, String)}
	 * checking wrong movements
	 */
	@Test
	public final void testUnknownMovement() {
		System.out.println("Unknown movements...");
		try {
			assertEquals("UnknownMovement 1: ", "",
					this.plateau.moveTiffi("44N", "LMXMRRM"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
		try {
			assertEquals("UnknownMovement 2: ", "",
					this.plateau.moveTiffi("44N", "LM?MRRM"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
		try {
			assertEquals("UnknownMovement 3: ", "",
					this.plateau.moveTiffi("44W", "LM MRRM"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
		try {
			assertEquals("UnknownMovement 4: ", "",
					this.plateau.moveTiffi("44N", "LM.MRRM"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link org.king.tiffi2mars.backend.Plateau#moveTiffi(String, String)}
	 * checking limit of movements
	 */
	@Test
	public final void testLimitMovements() {
		System.out.println("Limit of movements...");
		try {
			assertEquals("UnknownMovement 1: ", "", this.plateau.moveTiffi(
					"44N",
					"RRRRRRRRRRLLLLLLLLLLRRRRRRRRRRLLLLLLLLLLRRRRRRRRRRL"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link org.king.tiffi2mars.backend.Plateau#moveTiffi(String, String)}
	 * checking wrong Landing Positions
	 */
	@Test
	public final void testWrongPosition() {
		System.out.println("Wrong landing positions...");
		try {
			assertEquals("WrongPosition 1: ", "-93N",
					this.plateau.moveTiffi("-93N", "RLRLRL"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
		try {
			assertEquals("WrongPosition 2: ", "109N",
					this.plateau.moveTiffi("109N", "RLRRLR"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
		try {
			assertEquals("WrongPosition 3: ", "00Z",
					this.plateau.moveTiffi("00Z", "RLRLR"));
		} catch (PlateauException e) {
			System.out.println("Catching PlateauException: " + e.getMessage());
		}
	}

}
